REM pip install -r python_packages.txt



rem pip install streamlit pillow excel2json-3 xlwings openpyxl jupyter notebook pdfminer schedule spacy  pendulum

pip install colorlog

REM UI
pip install PyQt5
REM UI that works on ios, Android, lunix, windows
pip install kivi

REM Data Science
Rem work with matrix
pip install numpy
rem pip install scipy
rem pip install sympy
REM work with data frame
pip install pandas
REM data visualization
pip install matplotlib
rem more advanced thatn matplotlib, to draw graphs
pip install plotly
rem web based because uses HTML and JS 
pip install bokeh

REM machine learning and AI
REM from Google, neural network
rem pip install tensorflow
REM higher level API for Tensorflow, supported by Google, run on CPU and GPU
rem pip install keras
REM ML and AI
rem pip install pytorch
REM SCI KIT Learn
rem pip install sklearn 


REM text processing, sentence, language; tokenize a sentence
pip install nltk
rem pip install gensim
rem pip install flashtext

REM web scrapping
pip install beautifulsoup4
rem pip install scrapy


REM web framework
REM light web server
pip install flask
rem similar to Flask
rem pip install django


REM automation on website - test, or interact with other website, move curosr, click button
REM pip install selenium



REM image recognition, video data processing, object recognition and detection, draw on images, videos
REM face detector, add makup
rem pip install opencv-python


