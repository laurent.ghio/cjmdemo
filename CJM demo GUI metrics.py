#! python3
# -*- coding:Utf-8 -*-

import sys
import os
import csv
import json
import mimetypes
import http.client
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtWidgets import (QFileDialog,
                             QMainWindow, QLabel, QWidget, QGridLayout,
                             QLineEdit, QVBoxLayout,
                             QHBoxLayout, QPushButton, QGroupBox,
                             QRadioButton, QCheckBox,
                             QComboBox, QColorDialog, QStyleFactory,
                             QScrollArea, QWidgetItem, QDockWidget, QAction,
                             qApp, QMessageBox, QPlainTextEdit, QToolBar, QDialog)
from PyQt5.QtCore import Qt, QCoreApplication, QSettings, QSize, QByteArray, QBuffer, QIODevice
from PyQt5.QtGui import (QColor, QPalette, QIcon, QFont, QPainter,
                         QBrush, QPixmap, QImage, QTextCursor)
import configparser
import logging
import colorlog
import pandas

# ---------------------------------------------------------
#   CJM DEMO TOOL
#
#   - upload HTML to Embedded Component within the map
#   - Create/Change KPI attached to Touch Point
#   - Run a demo script with multiple actions to update several objects at once in the map
#
# width: which is allowed 3,6,9,12,15,20,26,32,40
# color code: #aeaeae, #69b027, #f28918, #e34243
# linePattern : SolidLine, Dashed, or Dotted
# arrowDirection : Forward, Backward, or Both.
#
#       LOADHTML,mapID,componentID,htmlFile
#       LINE,mapId,lineID,text,thickness,color,pattern,arrow
#       CREATEKPI,touchpointID,metricKey,metricName,metricValue
#       DELKPI,touchpointID,metricKey
#       TPSCORE,touchpointID,feelingScore,label
#
#		RESETDATA
#		STACKDATA,dataSetKey,dataPointKey,value
#		SENDDATA
# -----------------------------------------------------------

VERSION = 9.5

# ---------------------------------------------------------
# CHANGELOG
#
#   - 9.3 - added log file
#   - 9.4 - added Data Set, Data Point
#   - 9.5 - more info in the log + Excel data file
# ---------------------------------------------------------

# global variables
cloudAccount = {}


class classCloudAccount():
    def __init__(self):
        
        self.applicationPath = ""
        self.imagePath = ""
        self.configPath = ""
        self.logFile = ""

        self.envName = "no env name yet"
        self.filenameCFGFullPath = ""
        self.cfgSection = "default"
        self.URLCJM = "not config loaded yet"
        self.apiKey = "no api key yet"
        self.dirHTMLRoot = "no HTML folder yet"
        self.dirCSVRoot = "no CSV folder yet"
        logging.debug(f'Env : {self.envName}')

        self.mapID = "601204204"
        self.touchpointID = "601204212"
        self.feelingLabel = "I like it"
        self.feelingScore = "3"
        self.componentID = "601204218"
        self.metricName = "NPS"
        self.metricLabel = "NPS Score October 2020"
        self.metricValue = "34"
        self.fileName = "DIALOG"
        self.excelFileName = "DIALOG"
        self.HTMLContent = ""
        self.lineID = "lineID"
        self.lineLabel = "76%"
        self.lineWidth = "3"
        self.linePattern = "SolidLine"
        self.lineArrow = "Forward"
        self.lineColor = "#aeaeae"

        self.dataSetKey = "nameofdataset"
        self.dataPointKey = "nameofdatapoint"
        self.dataPointValue = "valueofdatapoint"

        self.dataPointList = []

    def setNewEnv(self, fullpathfilename, section):
        try:
            # filename = fct_configFullPath(relative_filename)
            logging.debug(f'==>setNewEnv(), {fullpathfilename}, {section}')
            cfg = configparser.ConfigParser()
            cfg.read(fullpathfilename)

            if (section == "default"):
                print("Load default section of the config file")
                listEnv = cfg.sections()
                section = listEnv[0]
                logging.debug(f'first section of the config file={section}')

            self.cfgSection = section
            config = cfg[section]

            self.envName = config.get('envName', 'default env name')
            self.URLCJM = config.get('URLCJM', 'default')
            self.apiKey = config.get('apiKey', 'default')
            self.dirHTMLRoot = config.get('dirHTMLRoot', 'default')
            self.dirCSVRoot = config.get('dirCSVRoot', 'default')
            self.mapID = config.get('mapID', 'default')
            self.touchpointID = config.get('touchpointID', 'default')
            self.feelingLabel = config.get('feelingLabel', 'default')
            self.feelingScore = config.get('feelingScore', 'default')
            self.componentID = config.get('componentID', 'default')
            self.metricName = config.get('metricName', 'default')
            self.metricLabel = config.get('metricLabel', 'default')
            self.metricValue = config.get('metricValue', 'default')
            self.lineID = config.get('lineID', 'default')
            self.lineLabel = config.get('lineLabel', 'default')
            self.lineWidth = config.get('lineWidth', 'default')
            self.linePattern = config.get('linePattern', 'default')
            self.lineArrow = config.get('lineArrow', 'default')
            self.lineColor = config.get('lineColor', 'default')
            self.dataSetKey = config.get('dataSetKey', 'default')
            self.dataPointKey = config.get('dataPointKey', 'default')
            self.dataPointValue = config.get('dataPointValue', 'default')


        except:  # catch *all* exceptions
            e = sys.exc_info()[0]
            logging.error(f'Error={e}')

    def getListCloudAccount(self, fullpathfilename):
        logging.debug(f'==>getListCloudAccount(), {fullpathfilename}')
        cfg = configparser.ConfigParser()
        cfg.read(fullpathfilename)
        listEnv = cfg.sections()
        logging.debug(f'list of cloud accounts in the config file={listEnv}')
        return(listEnv)








# -----------------------------------------------------------
def fct_imageFullPath(relative_path):
    global cloudAccount

    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS,
        # and places our data files in a folder relative to that temp
        # folder named as specified in the datas tuple in the spec file
        imageFullPath = os.path.join(
            sys._MEIPASS, 'images') + '/' + relative_path
    except Exception:
        imageFullPath = cloudAccount.imagePath + '/' + relative_path
    logging.debug(f'imageFullPath={imageFullPath}')

    return imageFullPath


def fct_configFullPath(relative_path):
    global cloudAccount

    configFullPath = cloudAccount.configPath + '/' + relative_path
    logging.debug(f'configFullPath={configFullPath}')

    return configFullPath


def fct_callCJM(apiName, apiPayload):
    global cloudAccount
    try:
        logging.debug(f'apiName={apiName}')
        logging.debug(f'apiPayload={apiPayload}')
        logging.debug(f'URLCJM={cloudAccount.URLCJM}')

        conn = http.client.HTTPSConnection(cloudAccount.URLCJM)
        apiHeader = {
            'Authorization': 'Bearer ' + cloudAccount.apiKey,
            'Content-Type': 'application/json'
        }
        logging.debug(f'apiHeader={apiHeader}')
        logging.debug("URL CJM="+cloudAccount.URLCJM+":"+"apiName=" +
                      apiName+":"+"apiPayload="+str(apiPayload)+":"+"apiHeader="+str(apiHeader))
        conn.request("POST", apiName, apiPayload, apiHeader)
        res = conn.getresponse()
        data = res.read()
        # fct_displayResult(data.decode("utf-8"))
        result = data.decode("utf-8")
        logging.debug(f'result={result}')
        return(result)
    except ValueError:
        pass


'''
def fct_loadTask(*args):
    try:
        taskFilename = filedialog.askopenfilename(filetypes=[(
            "Task list", "*.task")], initialdir=dirHTMLRoot, title="Select Task List CSV file", defaultextension='.csv')
        # taskFilename = dirHTMLRoot + "/" + taskFilename;
        print("taskFilename="+taskFilename)
        with open(taskFilename, 'r') as csvFile:
            csvContent = csv.DictReader(csvFile)
            print("CSV content= " + str(csvContent))
            text_contentValue.delete("1.0", END)
            for row in csvContent:
                text_contentValue.insert(END, row)
                # Name Name,Description,Project,Effort,Benefit,Link,Status
                print("==> Name: " + row['Name'])
                print("==> Description: " + row['Description'])
                print("==> Project: " + row['Project'])
                print("==> Effort: " + row['Effort'])
                print("==> Benefit: " + row['Benefit'])
                print("==> Link: " + row['Link'])
                print("==> Status: " + row['Status'])
            apiCmd = "/api/publish/Cjm/PublicAddTaskCommandV3"
            payload = " {" + "name:\"" + row['Name'] + "\"," \
                + "description:\"" + row['Description'] + "\"," \
                + "project:\"" + row['Project'] + "\"," \
                + "effort:" + row['Effort'] + "," \
                + "benefit:" + row['Benefit'] + "," \
                + "link:\"" + row['Link'] + "\"," \
                + "status:\"" + row['Status'] + "\"" \
                + "}"
            result = fct_callCJM(apiCmd, payload)


    except ValueError:
        pass
'''

# -------------------------------------------------------------------------
#  GUI - DISPLAY
# -------------------------------------------------------------------------


class MainWindow(QMainWindow):
    global classCloudAccount

    def __init__(self, app):
        QMainWindow.__init__(self)  # call the ancestor
        self.app = app
        self.initGui()

    def initGui(self):
        self.setWindowTitle("CJM Demo: "+cloudAccount.URLCJM)
        # print("taille ecran=", self.app.primaryScreen().size())
        # width, height
        self.resize(850, self.app.primaryScreen().size().height()-100)
        self.move(0, 0)

        self.statusBar().showMessage(
            'Simulate Backoffice Application calling CJM SaaS solution')

        self.layoutWindow = QVBoxLayout()
        self.layoutParams = QGridLayout()
        self.layoutContent = QGridLayout()

        # --------------------------------------------
        # Menu
        #
        menu = self.menuBar()

        fileMenu = menu.addMenu("File")

        actionLoadCfg = QAction(
            QIcon(fct_imageFullPath('loadcfg.png')), "Load new environment settings *.cfg ...", self)
        actionLoadCfg.triggered.connect(self.fct_loadCfgScriptDialog)
        fileMenu.addAction(actionLoadCfg)

        actionLoadDemo = QAction(
            QIcon(fct_imageFullPath('loaddemo.png')), "Load Demo file (CSV) and execute it...", self)
        actionLoadDemo.triggered.connect(self.fct_loadDemoScriptDialog)
        fileMenu.addAction(actionLoadDemo)

        actionLoadExcelData = QAction(
            QIcon(fct_imageFullPath('loadexcel.png')), "Load Excel file (XLSX) to CJM Data Repository...", self)
        actionLoadExcelData.triggered.connect(self.fct_loadExcelData)
        fileMenu.addAction(actionLoadExcelData)

        fileMenu.addSeparator()

        actionExit = QAction(
            QIcon(fct_imageFullPath('exit.png')), "Exit", self)
        actionExit.triggered.connect(self.close)
        fileMenu.addAction(actionExit)

        runMenu = menu.addMenu("Run")
        # print("reserdemo.png path=", fct_imageFullPath('resetdemo.jpg')
        actionResetDemo = QAction(
            QIcon(fct_imageFullPath('resetdemo.png')), "Reset Demo", self)
        actionResetDemo.triggered.connect(self.fct_loadDemoScriptDemoReset)
        runMenu.addAction(actionResetDemo)

        actionRunDemo1 = QAction(
            QIcon(fct_imageFullPath('rundemo1.png')), "Run Demo 1", self)
        actionRunDemo1.triggered.connect(self.fct_loadDemoScriptDemo1)
        runMenu.addAction(actionRunDemo1)

        actionRunDemo2 = QAction(
            QIcon(fct_imageFullPath('rundemo2.png')), "Run Demo 2", self)
        actionRunDemo2.triggered.connect(self.fct_loadDemoScriptDemo2)
        runMenu.addAction(actionRunDemo2)

        displayMenu = menu.addMenu("Display")

        actionToggleParams = QAction(
            "Show/Hide parameters", self, checkable=True)
        actionToggleParams.triggered.connect(self.fct_toggleParameters)
        actionToggleParams.setChecked(False)
        displayMenu.addAction(actionToggleParams)

        '''
        actionHideParams = QAction(
            QIcon(fct_imageFullPath('hide_params.png')), "Hide parameters", self)
        actionHideParams.triggered.connect(self.fct_hideParameters)
        displayMenu.addAction(actionHideParams)

        actionShowParams = QAction(
            QIcon(fct_imageFullPath('show_params.png')), "Show parameters", self)
        actionShowParams.triggered.connect(self.fct_showParameters)
        displayMenu.addAction(actionShowParams)
        '''

        actionClearInfo = QAction(
            QIcon(fct_imageFullPath('clear_info.png')), "Clear info", self)
        actionClearInfo.triggered.connect(self.fct_displayInfoClear)
        displayMenu.addAction(actionClearInfo)

        actionClearResult = QAction(
            QIcon(fct_imageFullPath('clear_result.png')), "Clear result", self)
        actionClearResult.triggered.connect(self.fct_displayResultClear)
        displayMenu.addAction(actionClearResult)

        self.envMenu = menu.addMenu("Environments")
        self.fct_addMenuEnv()

        aboutMenu = menu.addMenu("About")
        actionCredits = QAction(
            QIcon(fct_imageFullPath('version.png')), "Version", self)
        actionCredits.triggered.connect(self.fct_credits)
        aboutMenu.addAction(actionCredits)

        # --------------------------------------------
        # Toolbar
        #

        toolbarBox = QToolBar('')
        self.addToolBar(Qt.LeftToolBarArea, toolbarBox)
        toolbarBox.addAction(actionLoadDemo)
        toolbarBox.addAction(actionLoadExcelData)
        toolbarBox.addSeparator()
        toolbarBox.addAction(actionResetDemo)
        toolbarBox.addAction(actionRunDemo1)
        toolbarBox.addAction(actionRunDemo2)
        toolbarBox.addSeparator()
        toolbarBox.addAction(actionClearInfo)
        toolbarBox.addAction(actionClearResult)
        toolbarBox.addSeparator()

        # ----------------------------------------------------------
        # dockable parameters
        #
        self.dockParams = QDockWidget("Parameters", self)
        # self.dockParams.setStyleSheet("QDockWidget QLabel {background: red;}")
        self.dockParams.setStyleSheet(
            """
            QDockWidget QLabel {padding: 5px;}
            """)
        # self.dockParams.setFloating(False)
        self.addDockWidget(Qt.TopDockWidgetArea, self.dockParams)
        self.dockedWidgets = QWidget(self)
        self.dockParams.setWidget(self.dockedWidgets)
        self.dockedWidgets.setLayout(QVBoxLayout())
        # allWidgetsParam = QVBoxLayout()

        # URL Cloud account selected
        self.labelURLCloudAccount = QLabel(
            "URL Cloud account: \n"+cloudAccount.URLCJM)
        self.dockedWidgets.layout().addWidget(self.labelURLCloudAccount)

        # CSV directory - where to find the CSV scripts
        self.labelCSVDir = QLabel("CSV folder: \n"+cloudAccount.dirCSVRoot)
        self.dockedWidgets.layout().addWidget(self.labelCSVDir)

        # HTML directory - where to find the graphics in HTML format
        self.labelHTMLDir = QLabel("HTML folder: \n"+cloudAccount.dirHTMLRoot)
        self.dockedWidgets.layout().addWidget(self.labelHTMLDir)

        # Api Key
        self.labelApiKey = QLabel("API Key: \n"+cloudAccount.apiKey)
        self.dockedWidgets.layout().addWidget(self.labelApiKey)

        # force les widgets déjà positionnés à se retrouver en haut de la zone
        self.dockedWidgets.layout().addStretch()
        self.fct_hideParameters()  # hide the parameters by default

        # -----------------------------------------------------
        # ----------------------------------
        # main content zone
        self.widgetContent = QWidget()
        self.widgetContent.setLayout(self.layoutContent)

        # push all content to the top of the window by adding a strecher
        # self.layoutContent.addStretch()

        dynRow = 0
        # input fields + buttons
        self.labelMapID = QLabel("Map ID:")
        self.layoutContent.addWidget(self.labelMapID, dynRow, 0)
        self.editMapID = QLineEdit(self)
        self.editMapID.setText(cloudAccount.mapID)
        self.editMapID.textChanged.connect(self.onMapIDChanged)
        self.layoutContent.addWidget(self.editMapID, dynRow, 1)

        # dynRow += 1
        self.componentID = QLabel("Component ID:")
        self.layoutContent.addWidget(self.componentID, dynRow, 2)
        self.editComponentID = QLineEdit(self)
        self.editComponentID.setText(cloudAccount.componentID)
        self.editComponentID.textChanged.connect(
            self.onComponentIDLabelChanged)
        self.layoutContent.addWidget(self.editComponentID, dynRow, 3)

        dynRow += 1
        self.labelTouchpointID = QLabel("Touchpoint ID:")
        self.layoutContent.addWidget(self.labelTouchpointID, dynRow, 0)
        self.editTouchpointID = QLineEdit(self)
        self.editTouchpointID.setText(cloudAccount.touchpointID)
        self.editTouchpointID.textChanged.connect(self.onTouchpointIDChanged)
        self.layoutContent.addWidget(self.editTouchpointID, dynRow, 1)

        # dynRow += 1
        self.getTouchpointMetrics = QPushButton(
            "  Get TP Metrics\n  (touchpointID)")
        self.getTouchpointMetrics.setIcon(
            QIcon(fct_imageFullPath('TPMetrics.png')))
        self.layoutContent.addWidget(
            self.getTouchpointMetrics, dynRow, 2, 1, 2)
        self.getTouchpointMetrics.clicked.connect(
            self.fct_get_tp_metrics)

        dynRow += 1
        self.labelFeelingScore = QLabel("Feeling Score:")
        self.layoutContent.addWidget(self.labelFeelingScore, dynRow, 0)
        self.comboFeelingScore = QComboBox()
        self.comboFeelingScore.addItems(
            ["-5", "-4", "-3", "-2", "-1", "0", "1", "2", "3", "4", "5"])
        self.comboFeelingScore.activated[str].connect(
            self.onChangedTouchpointFeelingScore)
        self.comboFeelingScore.setCurrentIndex(3)
        self.layoutContent.addWidget(self.comboFeelingScore, dynRow, 1)

        # dynRow += 1
        self.feelingLabel = QLabel("Feeling Label:")
        self.layoutContent.addWidget(self.feelingLabel, dynRow, 2)
        self.editFeelingLabel = QLineEdit(self)
        self.editFeelingLabel.setText(cloudAccount.feelingLabel)
        self.editFeelingLabel.textChanged.connect(self.onFeelingLabelChanged)
        self.layoutContent.addWidget(self.editFeelingLabel, dynRow, 3)

        dynRow += 1
        self.updateTouchpointFeelingScore = QPushButton(
            "  Update TP Feeling Score\n  (touchpointID, feelingScore, feelingLabel)")
        self.updateTouchpointFeelingScore.setIcon(
            QIcon(fct_imageFullPath('feeling.png')))
        self.layoutContent.addWidget(
            self.updateTouchpointFeelingScore, dynRow, 2, 1, 2)
        self.updateTouchpointFeelingScore.clicked.connect(
            self.fct_update_tp_score)

        dynRow += 1
        self.labelLineID = QLabel("Line ID:")
        self.layoutContent.addWidget(self.labelLineID, dynRow, 0)
        self.editLineID = QLineEdit(self)
        self.editLineID.setText(cloudAccount.lineID)
        self.editLineID.textChanged.connect(self.onLineIDChanged)
        self.layoutContent.addWidget(self.editLineID, dynRow, 1)

        self.lineLabel = QLabel("Line Label:")
        self.layoutContent.addWidget(self.lineLabel, dynRow, 2)
        self.editLineLabel = QLineEdit(self)
        self.editLineLabel.setText(cloudAccount.lineLabel)
        self.editLineLabel.textChanged.connect(self.onLineLabelChanged)
        self.layoutContent.addWidget(self.editLineLabel, dynRow, 3)

        dynRow += 1
        self.labelLineWidth = QLabel("Line Width:")
        self.layoutContent.addWidget(self.labelLineWidth, dynRow, 0)
        self.comboLineWidth = QComboBox()
        self.comboLineWidth.addItems(
            ["3", "6", "9", "12", "15", "20", "26", "32", "40"])
        self.comboLineWidth.activated[str].connect(
            self.onChangedLineWidth)
        self.comboLineWidth.setCurrentIndex(1)
        self.layoutContent.addWidget(self.comboLineWidth, dynRow, 1)

        # dynRow += 1
        self.labelLineColor = QLabel("Line Color:")
        self.layoutContent.addWidget(self.labelLineColor, dynRow, 2)
        self.comboLineColor = QComboBox()
        self.comboLineColor.addItems(
            ["#aeaeae", "#69b027", "#f28918", "#e34243"])
        self.comboLineColor.activated[str].connect(
            self.onChangedLineColor)
        self.comboLineColor.setCurrentIndex(1)
        self.layoutContent.addWidget(self.comboLineColor, dynRow, 3)

        dynRow += 1
        self.labelLinePattern = QLabel("Line Pattern:")
        self.layoutContent.addWidget(self.labelLinePattern, dynRow, 0)
        self.comboLinePattern = QComboBox()
        self.comboLinePattern.addItems(["SolidLine", "Dashed", "Dotted"])
        self.comboLinePattern.activated[str].connect(
            self.onChangedLinePattern)
        self.comboLinePattern.setCurrentIndex(1)
        self.layoutContent.addWidget(self.comboLinePattern, dynRow, 1)

        # dynRow += 1
        self.labelLineArrow = QLabel("Line Arrow:")
        self.layoutContent.addWidget(self.labelLineArrow, dynRow, 2)
        self.comboLineArrow = QComboBox()
        self.comboLineArrow.addItems(["Forward", "Backward", "Both"])
        self.comboLineArrow.activated[str].connect(
            self.onChangedLineArrow)
        self.comboLineArrow.setCurrentIndex(1)
        self.layoutContent.addWidget(self.comboLineArrow, dynRow, 3)

        dynRow += 1
        self.updateLine = QPushButton(
            "  Update Line\n  (mapID, lineID, all line attributes)")
        self.updateLine.setIcon(
            QIcon(fct_imageFullPath('line.png')))
        self.layoutContent.addWidget(
            self.updateLine, dynRow, 2, 1, 2)
        self.updateLine.clicked.connect(self.fct_updateLine)

        dynRow += 1
        self.metricName = QLabel("Metric Name:")
        self.layoutContent.addWidget(self.metricName, dynRow, 0)
        self.editMetricName = QLineEdit(self)
        self.editMetricName.setText(cloudAccount.metricName)
        self.editMetricName.textChanged.connect(self.onMetricNameLabelChanged)
        self.layoutContent.addWidget(self.editMetricName, dynRow, 1)

        # dynRow += 1
        self.metricLabel = QLabel("Metric Label:")
        self.layoutContent.addWidget(self.metricLabel, dynRow, 2)
        self.editMetricLabel = QLineEdit(self)
        self.editMetricLabel.setText(cloudAccount.metricLabel)
        self.editMetricLabel.textChanged.connect(
            self.onMetricLabelLabelChanged)
        self.layoutContent.addWidget(self.editMetricLabel, dynRow, 3)

        dynRow += 1
        self.metricValue = QLabel("Metric Value:")
        self.layoutContent.addWidget(self.metricValue, dynRow, 0)
        self.editMetricValue = QLineEdit(self)
        self.editMetricValue.setText(cloudAccount.metricValue)
        self.editMetricValue.textChanged.connect(
            self.onMetricValueLabelChanged)
        self.layoutContent.addWidget(self.editMetricValue, dynRow, 1)

        # dynRow += 1
        self.delTouchpointMetrics = QPushButton(
            "Delete Metric\n(touchpointID, metricName)")
        self.layoutContent.addWidget(
            self.delTouchpointMetrics, dynRow, 2, 1, 2)
        self.delTouchpointMetrics.clicked.connect(
            self.fct_delete_tp_metrics)

        dynRow += 1
        self.createTouchpointMetrics = QPushButton(
            "Create Metric\n(touchpointID, metricName, metricLabel, metricValue)")
        self.layoutContent.addWidget(
            self.createTouchpointMetrics, dynRow, 0, 1, 2)
        self.createTouchpointMetrics.clicked.connect(
            self.fct_create_tp_metrics)

        # dynRow += 1
        self.updateTouchpointMetrics = QPushButton(
            "Update Metric\n(touchpointID, metricName, metricLabel, metricValue)")
        self.layoutContent.addWidget(
            self.updateTouchpointMetrics, dynRow, 2, 1, 2)
        self.updateTouchpointMetrics.clicked.connect(
            self.fct_update_tp_metrics)

        dynRow += 1
        self.loadHTML = QPushButton(
            "  Load HTML")
        self.loadHTML.setIcon(
            QIcon(fct_imageFullPath('loadhtml.png')))
        self.layoutContent.addWidget(self.loadHTML, dynRow, 0, 1, 2)
        self.loadHTML.clicked.connect(
            self.fct_loadHTML)

        # dynRow += 1
        self.uploadHTML = QPushButton(
            "  Upload HTML\n  (mapID, componentID)")
        self.uploadHTML.setIcon(
            QIcon(fct_imageFullPath('upload.png')))
        self.layoutContent.addWidget(self.uploadHTML, dynRow, 2, 1, 2)
        self.uploadHTML.clicked.connect(
            self.fct_update_HTML_embed)

        dynRow += 1
        self.labelDataSetKey = QLabel("dataSet Key:")
        self.layoutContent.addWidget(self.labelDataSetKey, dynRow, 0)
        self.editDataSetKey = QLineEdit(self)
        self.editDataSetKey.setText(cloudAccount.dataSetKey)
        self.editDataSetKey.textChanged.connect(self.onDataSetKeyChanged)
        self.layoutContent.addWidget(self.editDataSetKey, dynRow, 1)

        self.labelDataPointKey = QLabel("dataPoint Key:")
        self.layoutContent.addWidget(self.labelDataPointKey, dynRow, 2)
        self.editDataPointKey = QLineEdit(self)
        self.editDataPointKey.setText(cloudAccount.dataPointKey)
        self.editDataPointKey.textChanged.connect(self.onDataPointKeyChanged)
        self.layoutContent.addWidget(self.editDataPointKey, dynRow, 3)

        dynRow += 1
        self.labelDataPointValue = QLabel("dataPoint Value:")
        self.layoutContent.addWidget(self.labelDataPointValue, dynRow, 0)
        self.editDataPointValue = QLineEdit(self)
        self.editDataPointValue.setText(cloudAccount.dataPointValue)
        self.editDataPointValue.textChanged.connect(self.onDataPointValueChanged)
        self.layoutContent.addWidget(self.editDataPointValue, dynRow, 1)
        
        self.updateDataSet = QPushButton(
            "  Update data point\n  (dataSetKey, dataSetPoint, dataSetValue)")
        self.updateDataSet.setIcon(
            QIcon(fct_imageFullPath('upload.png')))
        self.layoutContent.addWidget(self.updateDataSet, dynRow, 2, 1, 2)
        self.updateDataSet.clicked.connect(
            self.fct_update_data_point)



        '''
        dynRow += 1
        self.demoReset = QPushButton(
            "Reset Demo")
        self.layoutContent.addWidget(self.demoReset, dynRow, 0, 1, 2)
        self.demoReset.clicked.connect(
            self.fct_loadDemoScriptDemoReset)

        dynRow += 1
        self.demoReset = QPushButton(
            "Run Demo 1")
        self.layoutContent.addWidget(self.demoReset, dynRow, 0, 1, 2)
        self.demoReset.clicked.connect(
            self.fct_loadDemoScriptDemo1)

        dynRow += 1
        self.demoReset = QPushButton(
            "Run Demo 2")
        self.layoutContent.addWidget(self.demoReset, dynRow, 0, 1, 2)
        self.demoReset.clicked.connect(
            self.fct_loadDemoScriptDemo2)

        dynRow += 1
        self.demoReset = QPushButton(
            "Load Demo")
        self.layoutContent.addWidget(self.demoReset, dynRow, 0, 1, 2)
        self.demoReset.clicked.connect(
            self.fct_loadDemoScriptDialog)
        '''

        # ------------ INFO ----------------
        dynRow += 1
        self.displayInfo = QPlainTextEdit()
        self.displayInfo.resize(400, 400)
        self.layoutContent.addWidget(self.displayInfo, dynRow, 0, 1, 4)

        # ------------ RESULTS ----------------
        dynRow += 1
        self.displayResult = QPlainTextEdit()
        self.displayResult.resize(400, 400)
        self.layoutContent.addWidget(self.displayResult, dynRow, 0, 1, 4)

        # -----------------------------------------------------------
        # add a scrollbar to the main content zone
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        # Scroll Area Properties
        # self.scrollArea.setStyleSheet("background : lightgreen;")
        # self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        # self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.widgetContent)

        # Display the stack of content in the main window
        self.layoutWindow.addLayout(self.layoutParams)
        self.layoutWindow.addWidget(self.scrollArea)
        widgetWindow = QWidget()
        widgetWindow.setLayout(self.layoutWindow)
        self.setCentralWidget(widgetWindow)

    def updateUI(self):
        print("==> updateUI()")
        self.setWindowTitle(
            "CJM Demo: "+cloudAccount.envName+" ==> "+cloudAccount.URLCJM+"["+cloudAccount.cfgSection+"]")
        self.fct_addMenuEnv()
        self.labelURLCloudAccount.setText(
            "URL Cloud account: \n"+cloudAccount.URLCJM)
        self.labelHTMLDir.setText("HTML folder: \n"+cloudAccount.dirHTMLRoot)
        self.editMapID.setText(cloudAccount.mapID)
        self.editComponentID.setText(cloudAccount.componentID)
        self.editTouchpointID.setText(cloudAccount.touchpointID)
        self.editFeelingLabel.setText(cloudAccount.feelingLabel)
        self.comboFeelingScore.setCurrentIndex(
            self.comboFeelingScore.findText(cloudAccount.feelingScore))
        self.editMetricName.setText(cloudAccount.metricName)
        self.editMetricLabel.setText(cloudAccount.metricLabel)
        self.editMetricValue.setText(cloudAccount.metricValue)
        self.editLineID.setText(cloudAccount.lineID)
        self.editLineLabel.setText(cloudAccount.lineLabel)
        print("line width=", cloudAccount.lineWidth)
        self.comboLineWidth.setCurrentIndex(
            self.comboLineWidth.findText(cloudAccount.lineWidth))
        self.comboLinePattern.setCurrentIndex(
            self.comboLinePattern.findText(cloudAccount.linePattern))
        self.comboLineColor.setCurrentIndex(
            self.comboLineColor.findText(cloudAccount.lineColor))
        self.comboLineArrow.setCurrentIndex(
            self.comboLineArrow.findText(cloudAccount.lineArrow))
        self.editDataSetKey.setText(cloudAccount.dataSetKey)
        
    def onMapIDChanged(self, text):
        cloudAccount.mapID = text
        print("new map ID=", cloudAccount.mapID)

    def onTouchpointIDChanged(self, text):
        cloudAccount.touchpointID = text
        print("new touchpoint ID=", cloudAccount.touchpointID)

    def onComponentIDLabelChanged(self, text):
        cloudAccount.componentID = text
        print("new component ID=", cloudAccount.componentID)

    def onMetricNameLabelChanged(self, text):
        cloudAccount.metricName = text
        print("new Metric Name=", cloudAccount.metricName)

    def onMetricLabelLabelChanged(self, text):
        cloudAccount.metricLabel = text
        print("new Metric Label=", cloudAccount.metricLabel)

    def onMetricValueLabelChanged(self, text):
        cloudAccount.metricValue = text
        print("new Metric Value=", cloudAccount.metricValue)

    def onChangedTouchpointFeelingScore(self, text):
        cloudAccount.feelingScore = text
        print("new feeling score=", cloudAccount.feelingScore)

    def onFeelingLabelChanged(self, text):
        cloudAccount.feelingLabel = text
        print("new feeling label=", cloudAccount.feelingLabel)

    def onLineIDChanged(self, text):
        cloudAccount.lineID = text
        print("new line ID=", cloudAccount.lineID)

    def onLineLabelChanged(self, text):
        cloudAccount.lineLabel = text
        print("new line label=", cloudAccount.lineLabel)

    def onChangedLineWidth(self, text):
        cloudAccount.lineWidth = text
        print("new line Width=", cloudAccount.lineWidth)

    def onChangedLineColor(self, text):
        cloudAccount.lineColor = text
        print("new line color=", cloudAccount.lineColor)

    def onChangedLinePattern(self, text):
        cloudAccount.linePattern = text
        print("new line pattern=", cloudAccount.linePattern)

    def onChangedLineArrow(self, text):
        cloudAccount.lineArrow = text
        print("new line arrow=", cloudAccount.lineArrow)

    def onDataSetKeyChanged(self, text):
        cloudAccount.dataSetKey = text
        print("new dataSetKey=", cloudAccount.dataSetKey)

    def onDataPointKeyChanged(self, text):
        cloudAccount.dataPointKey = text
        print("new dataPointKey=", cloudAccount.dataPointKey)

    def onDataPointValueChanged(self, text):
        cloudAccount.dataPointValue = text
        print("new dataPointValue=", cloudAccount.dataPointValue)

        
    def fct_displayResult(self, result):
        try:
            print("result=" + result)
            self.displayResult.clear()
            self.displayResult.insertPlainText(result)
        except ValueError:
            pass

    def fct_displayResultAppend(self, result):
        try:
            print("result=" + result)
            # self.displayInfo.clear()
            self.displayResult.appendPlainText(result)
            self.displayResult.moveCursor(QTextCursor.End)
            QtWidgets.QApplication.processEvents()

        except ValueError:
            pass

    def fct_displayResultClear(self):
        self.displayResult.clear()

    def fct_displayInfo(self, info):
        try:
            print("info=" + info)
            self.displayInfo.clear()
            self.displayInfo.insertPlainText(info)
        except ValueError:
            pass

    def fct_displayInfoAppend(self, info):
        try:
            print("info=" + info)
            # self.displayInfo.clear()
            self.displayInfo.appendPlainText(info)
            self.displayInfo.moveCursor(QTextCursor.End)
            QtWidgets.QApplication.processEvents()

        except ValueError:
            pass

    def fct_displayInfoClear(self):
        self.displayInfo.clear()

    def fct_hideParameters(self):
        self.dockParams.hide()

    def fct_toggleParameters(self, state):
        if state:
            self.dockParams.show()
        else:
            self.dockParams.hide()

    def fct_showParameters(self):
        self.dockParams.show()

    def fct_update_tp_score(self):
        try:
            print("==> fct_update_to_score() ")
            touchPointIDValue = cloudAccount.touchpointID
            # index = self.comboFeelingScore.currentIndex()
            updateTouchpointFeelingScore = cloudAccount.feelingScore
            print("updateTouchpointFeelingScore=",
                  updateTouchpointFeelingScore)
            # updateTouchpointFeelingScore = self.comboFeelingScore.itemData(
            #    index).toString()

            # touchPointFeelingLabel = str(field_touchPointFeelingLabel.get())
            # print("touchPointIDValue=" + touchPointIDValue)
            # print("score=" + updateTouchpointFeelingScore)
            touchPointFeelingLabel = cloudAccount.feelingLabel
            print("label=" + touchPointFeelingLabel)

            # apiCmd = "/api/publish/Cjm/PublicEditupdateTouchpointFeelingScoreCommand"
            apiCmd = "/api/publish/Cjm/PublicEditTouchPointFeelingScoreCommand"
            payload = "   {\n  \"touchPointId\": " + touchPointIDValue                \
                + ",\n  \"feelingScore\": " + updateTouchpointFeelingScore           \
                + ",\n  \"label\": " + "\"" + touchPointFeelingLabel + "\""  \
                + "\n}"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_get_tp_metrics(self):
        try:
            touchPointIDValue = cloudAccount.touchpointID
            print("touchPointIDValue=" + touchPointIDValue)

            apiCmd = "/api/query/Cjm/PublicListCustomTouchPointMetricsQuery"
            payload = "{\"touchPointId\": \"" + touchPointIDValue + "\"}"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResult(result)

        except ValueError:
            pass

    def fct_delete_tp_metrics(self):
        try:
            touchPointIDValue = cloudAccount.touchpointID
            metricName = cloudAccount.metricName
            print("touchPointIDValue=" + touchPointIDValue)
            print("metricName=" + metricName)

            apiCmd = "/api/publish/Cjm/PublicDeleteCustomTouchPointMetricCommand"
            payload = "{\n  \"touchPointId\": " + touchPointIDValue + \
                ",\n  \"key\": \"" + metricName + "\"\n}\n\n"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_create_tp_metrics(self):
        try:
            touchPointIDValue = cloudAccount.touchpointID
            metricName = cloudAccount.metricName
            metricLabel = cloudAccount.metricLabel
            metricValue = cloudAccount.metricValue
            print("touchPointIDValue=" + touchPointIDValue)
            print("metricName=" + metricName)
            print("metricLabel=" + metricLabel)
            print("metricValue=" + metricValue)

            apiCmd = "/api/publish/Cjm/PublicCreateCustomTouchPointMetricCommand"
            payload = "{\n  \"touchPointId\": " + touchPointIDValue + ",\n  \"key\": \"" + metricName + \
                "\",\n  \"label\": \""+metricLabel + \
                "\",\n  \"value\": \""+metricValue+"\"\n}\n\n"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

            if "already exists" in str(result):
                print("result: already exists, so just need to be updated")
                apiCmd = "/api/publish/Cjm//api/publish/Cjm/PublicUpdateCustomTouchPointMetricCommand"
                payload = "{\n  \"touchPointId\": " + touchPointIDValue + ",\n  \"key\": \"" + metricName + \
                    "\",\n  \"label\": \""+metricLabel + \
                    "\",\n  \"value\": \""+metricValue+"\"\n}\n\n"
                result = fct_callCJM(apiCmd, payload)
                self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_update_tp_metrics(self):
        try:
            print("==> fct_update_metrics()")
            touchPointIDValue = cloudAccount.touchpointID
            metricName = cloudAccount.metricName
            metricLabel = cloudAccount.metricLabel
            metricValue = cloudAccount.metricValue
            print("touchPointIDValue=" + touchPointIDValue)
            print("metricName=" + metricName)
            print("metricLabel=" + metricLabel)
            print("metricValue=" + metricValue)

            apiCmd = "/api/publish/Cjm/PublicUpdateCustomTouchPointMetricCommand"
            payload = "{\n  \"touchPointId\": " + touchPointIDValue + ",\n  \"key\": \"" + metricName + \
                "\",\n  \"label\": \""+metricLabel + \
                "\",\n  \"value\": \""+metricValue+"\"\n}\n\n"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_updateLine(self):
        try:
            mapIDValue = cloudAccount.mapID
            print("mapIDValue=" + mapIDValue)
            lineIDValue = cloudAccount.lineID
            print("lineIDValue=" + lineIDValue)
            lineLabelValue = cloudAccount.lineLabel
            print("lineLabelValue=" + lineLabelValue)
            lineWidthValue = cloudAccount.lineWidth
            print("lineWidthValue=" + lineWidthValue)
            lineColorValue = cloudAccount.lineColor
            print("lineColorValue=" + lineColorValue)
            linePatternValue = cloudAccount.linePattern
            print("linePatternValue=" + linePatternValue)
            lineArrowValue = cloudAccount.lineArrow
            print("lineArrowValue=" + lineArrowValue)

            apiCmd = "/api/publish/Cjm/PublicUpdateJourneyMapConnectionCommand"
            payload = "   {\n  \"programId\": " + "\"" + mapIDValue + "\""  \
                + ",\n  \"connectionID\": " + "\"" + lineIDValue + "\""  \
                + ",\n  \"label\": " + "\"" + lineLabelValue + "\""  \
                + ",\n  \"width\": " + "\"" + lineWidthValue + "\""  \
                + ",\n  \"linePattern\": " + "\"" + linePatternValue + "\""  \
                + ",\n  \"arrowDirection\": " + "\"" + lineArrowValue + "\""  \
                + ",\n  \"colorCode\": " + "\"" + lineColorValue + "\""  \
                + "\n}"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_loadFile(self, filename):
        print("==> fct_loadFile()", filename)
        try:
            with open(filename, 'r') as fileContent:
                content = fileContent.read()
            print("HTML content= " + content)
            return(content)
        except ValueError:
            pass

    def fct_loadHTML(self):
        try:
            getFilenameHTML = QFileDialog.getOpenFileName(
                None, "Choose a file", cloudAccount.dirHTMLRoot,
                filter="HTML Files(*.html)")
            # filenameHTML = filedialog.askopenfilename(filetypes=[(
            #    "HTML", "*.htm;*.html")], initialdir=dirHTMLRoot, title="Select HTML file", defaultextension='.html')
            filenameHTML = getFilenameHTML[0]
            print("filenameHTML=", filenameHTML)
            if (filenameHTML != ""):
                cloudAccount.HTMLContent = self.fct_loadFile(filenameHTML)
                self.fct_displayInfoAppend(cloudAccount.HTMLContent)
            else:
                print("No file name selected")
        except ValueError:
            pass

    def fct_update_HTML_embed(self):
        print("==> fct_update_HTML_embed()")
        try:
            print("push embedded code")

            mapIDValue = cloudAccount.mapID
            print("mapIDValue=" + mapIDValue)
            componentIDValue = cloudAccount.componentID
            print("componentIDValue=" + componentIDValue)
            # EmbedCodeValue = self.displayInfo.toPlainText()
            EmbedCodeValue = cloudAccount.HTMLContent
            print("embedCode 1=" + EmbedCodeValue)
            # EmbedCodeValue=EmbedCodeValue.encode('ascii', 'xmlcharrefreplace')
            EmbedCodeValue = json.dumps(EmbedCodeValue)
            # EmbedCodeValue=JSON.parse(JSON.stringify(EmbedCodeValue))
            # EmbedCodeValue=str(EmbedCodeValue.encode("utf8"))
            print("embedCode 2=" + EmbedCodeValue)

            apiCmd = "/api/publish/Cjm/PublicEditJourneyMapEmbedCodeComponentCommand"
            payload = "{\n  \"programId\": "+mapIDValue+",\n  \"componentId\": " + \
                componentIDValue+",\n  \"embedCode\": " + EmbedCodeValue + "\n}\n\n"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass

    def fct_update_data_point(self):
        try:
            print("==> fct_update_data_point()")
            dataSetKey = cloudAccount.dataSetKey
            dataPointKey = cloudAccount.dataPointKey
            dataValue = cloudAccount.dataPointValue
            print("dataSetKey=" + dataSetKey)
            print("dataPointKey=" + dataPointKey)
            print("dataValue=" + dataValue)

            apiCmd = "/api/publish/Cjm/PublicUpdateDataPointCommand"
            payload = "{\n \"items\": [ { \n  \"dataPointKey\": " + "\"" + dataPointKey + "\"" + ",\n  \"dataSetKey\": " + "\"" + dataSetKey + "\"" + \
                ",\n  \"value\": " + "\"" + dataValue + "\"" +"\n } \n ] \n } \n\n"
            result = fct_callCJM(apiCmd, payload)
            self.fct_displayResultAppend(result)

        except ValueError:
            pass


    def fct_loadDemoScriptDialog(self):
        cloudAccount.fileName = "DIALOG"
        self.fct_loadDemoScript()

    def fct_loadExcelmData(self):
        cloudAccount.excelFileName = "DIALOG"
        self.fct_loadExcelData()

    def fct_loadDemoScriptDemoReset(self):
        cloudAccount.fileName = "demoreset.csv"
        self.fct_loadDemoScript()

    def fct_loadDemoScriptDemo1(self):
        cloudAccount.fileName = "demo1.csv"
        self.fct_loadDemoScript()

    def fct_loadDemoScriptDemo2(self):
        cloudAccount.fileName = "demo2.csv"
        self.fct_loadDemoScript()

    # def fct_loadDemoScript(self, demoFilename):
    def fct_loadDemoScript(self):
        print("==> fct_loadDemoScript()")
        self.fct_displayInfoAppend("==> fct_loadDemoScript()")

        demoFilename = cloudAccount.fileName
        self.fct_displayInfoAppend("Demo Name="+demoFilename)

        try:
            print("Demo Name="+demoFilename)

            if demoFilename == "DIALOG":
                filenameCSV = QFileDialog.getOpenFileName(
                    None, "Choose a file", cloudAccount.dirCSVRoot,
                    filter="Demo CSV file (*.csv)")
                filenameCSV = filenameCSV[0]
            else:
                filenameCSV = cloudAccount.dirCSVRoot + "/" + demoFilename
            print("filename CSV=", filenameCSV)
            if filenameCSV == "":
                return

            with open(filenameCSV, 'r') as csvFile:
                csvContent = csv.DictReader(csvFile)
                # print("CSV content= " + str(csvContent))
                # self.displayInfo.clear()
                nbline = 1
                for row in csvContent:
                    # self.displayInfo.insertPlainText(row)
                    # Name
                    print("Action " + str(nbline) + " = " + row['Action'])
                    self.fct_displayInfoAppend(
                        "Action " + str(nbline) + " = " + str(row))
                    self.fct_displayResultAppend(
                        "Action " + str(nbline) + " = " + str(row))
                    nbline = nbline+1
                    if row['Action'].startswith(("#")):
                        print('=====> comment: '+row['Action'])

                    if row['Action'].startswith(("DELKPI")):
                        print("======> Delete KPI")
                        params = {}
                        params['field_touchpointID'] = row['Param1']
                        # field_touchpointID.delete(0, END)
                        # field_touchpointID.insert(0, params['field_touchpointID'])
                        params['field_MetricName'] = row['Param2']
                        # field_MetricName.delete(0, END)
                        # field_MetricName.insert(0, params['field_MetricName'])

                        apiCmd = "/api/publish/Cjm/PublicDeleteCustomTouchPointMetricCommand"
                        payload = "{\n  \"touchPointId\": " + params['field_touchpointID'] + \
                            ",\n  \"key\": \"" + \
                            params['field_MetricName'] + "\"\n}\n\n"
                        result = fct_callCJM(apiCmd, payload)
                        self.fct_displayResultAppend(result)

                    if row['Action'].startswith(("TPSCORE")):
                        print("======> TP Feeling Score")
                        params = {}
                        # apiCmd = "/api/publish/Cjm/PublicEditupdateTouchpointFeelingScoreCommand"
                        apiCmd = "/api/publish/Cjm/PublicEditTouchPointFeelingScoreCommand"
                        payload = "    {\n  \"touchPointId\": " + row['Param1']           \
                            + ",\n  \"feelingScore\": " + row['Param2']           \
                            + ",\n  \"label\": " + "\"" + row['Param3'] + "\""  \
                            + "\n}"
                        result = fct_callCJM(apiCmd, payload)
                        self.fct_displayResultAppend(result)

                    if row['Action'].startswith(("CREATEKPI")):
                        print("======> Create KPI")
                        params = {}
                        params['field_touchpointID'] = row['Param1']
                        # field_touchpointID.delete(0, END)
                        # field_touchpointID.insert(0, params['field_touchpointID'])
                        params['field_MetricName'] = row['Param2']
                        # field_MetricName.delete(0, END)
                        # field_MetricName.insert(0, params['field_MetricName'])
                        params['field_MetricLabel'] = row['Param3']
                        # field_MetricLabel.delete(0, END)
                        # field_MetricLabel.insert(0, params['field_MetricLabel'])
                        params['field_MetricValue'] = row['Param4']
                        # field_MetricValue.delete(0, END)
                        # field_MetricValue.insert(0, params['field_MetricValue'])
                        print(str(params))

                        apiCmd = "/api/publish/Cjm/PublicCreateCustomTouchPointMetricCommand"
                        payload = "{\n  \"touchPointId\": " + params['field_touchpointID'] + ",\n  \"key\": \"" + params['field_MetricName'] + \
                            "\",\n  \"label\": \"" + \
                            params['field_MetricLabel'] + "\",\n  \"value\": \"" + \
                            params['field_MetricValue'] + "\"\n}\n\n"
                        result = fct_callCJM(apiCmd, payload)
                        self.fct_displayResultAppend(result)

                        if "already exists" in str(result):
                            print(
                                "result: already exists, so just need to be updated")
                            apiCmd = "/api/publish/Cjm/PublicUpdateCustomTouchPointMetricCommand"
                            payload = "{\n  \"touchPointId\": " + params['field_touchpointID'] + ",\n  \"key\": \"" + params['field_MetricName'] + \
                                "\",\n  \"label\": \"" + \
                                params['field_MetricLabel']+"\",\n  \"value\": \"" + \
                                params['field_MetricValue']+"\"\n}\n\n"
                            result = fct_callCJM(apiCmd, payload)
                            self.fct_displayResultAppend(result)

                    if row['Action'].startswith(("UPDATEKPI")):
                        print("======> Create KPI")
                        params = {}
                        params['field_touchpointID'] = row['Param1']
                        # field_touchpointID.delete(0, END)
                        # field_touchpointID.insert(0, params['field_touchpointID'])
                        params['field_MetricName'] = row['Param2']
                        # field_MetricName.delete(0, END)
                        # field_MetricName.insert(0, params['field_MetricName'])
                        params['field_MetricLabel'] = row['Param3']
                        # field_MetricLabel.delete(0, END)
                        # field_MetricLabel.insert(0, params['field_MetricLabel'])
                        params['field_MetricValue'] = row['Param4']
                        # field_MetricValue.delete(0, END)
                        # field_MetricValue.insert(0, params['field_MetricValue'])
                        print(str(params))

                        apiCmd = "/api/publish/Cjm/PublicUpdateCustomTouchPointMetricCommand"
                        payload = "{\n  \"touchPointId\": " + params['field_touchpointID'] + ",\n  \"key\": \"" + params['field_MetricName'] + \
                            "\",\n  \"label\": \"" + \
                            params['field_MetricLabel']+"\",\n  \"value\": \"" + \
                            params['field_MetricValue']+"\"\n}\n\n"
                        result = fct_callCJM(apiCmd, payload)
                        self.fct_displayResultAppend(result)

                    if row['Action'].startswith(("LINE")):
                        print("==> Param5: " + row['Param5'])
                        print("======> Update Line")
                        params = {}
                        params['field_mapID'] = row['Param1']
                        # field_mapID.delete(0, END)
                        # field_mapID.insert(0, params['field_mapID'])
                        params['lineIDValue'] = row['Param2']
                        # field_lineID.delete(0, END)
                        # field_lineID.insert(0, params['lineIDValue'])
                        params['field_lineLabel'] = row['Param3']
                        # field_lineLabel.delete(0, END)
                        # field_lineLabel.insert(0, params['field_lineLabel'])
                        print(str(params))

                        apiCmd = "/api/publish/Cjm/PublicUpdateJourneyMapConnectionCommand"
                        payload = "   {\n  \"programId\": " + "\"" + params['field_mapID'] + "\""  \
                            + ",\n  \"connectionID\": " + "\"" + params['lineIDValue'] + "\""  \
                            + ",\n  \"label\": " + "\"" + params['field_lineLabel'] + "\""  \
                            + ",\n  \"width\": " + "\"" + row['Param4'] + "\""  \
                            + ",\n  \"colorCode\": " + "\"" + row['Param5'] + "\""  \
                            + ",\n  \"linePattern\": " + "\"" + row['Param6'] + "\""  \
                            + ",\n  \"arrowDirection\": " + "\"" + row['Param7'] + "\""  \
                            + "\n}"
                        result = fct_callCJM(apiCmd, payload)
                        self.fct_displayResultAppend(result)

                    if row['Action'].startswith(("LOADHTML")):
                        print(" loadhtml action=" + row['Action'])
                        print("======> LOAD HTML")
                        params = {}
                        cloudAccount.mapID = params['mapID'] = row['Param1']
                        # field_mapID.delete(0, END)
                        # field_mapID.insert(0, params['mapID'])
                        cloudAccount.componentID = params['componentID'] = row['Param2']
                        # field_componentID.delete(0, END)
                        # field_componentID.insert(0, params['componentID'])
                        params['filename'] = row['Param3']
                        # field_MetricLabel.delete(0, END)
                        # field_MetricLabel.insert(0, params['filename'])
                        print(str(params))
                        cloudAccount.HTMLContent = self.fct_loadFile(
                            cloudAccount.dirHTMLRoot + "/" + params['filename'])
                        self.fct_displayInfoAppend(cloudAccount.HTMLContent)
                        self.fct_update_HTML_embed()

                    if row['Action'].startswith(("RESETDATA")):
                        self.dataPointList = []

                    if row['Action'].startswith(("STACKDATA")):
                        print("======> Stack Data")
                        self.dataPointList.append("{ \"dataSetKey\": " + "\"" + row['Param1'] + "\"" + ",  \"dataPointKey\": " + "\"" + row['Param2'] + "\"" + \
                ",  \"value\": " + "\"" + row['Param3'] + "\"" + "}")
                        print(self.dataPointList)

                    if row['Action'].startswith(("SENDDATA")):
                        print("dataPointList=",str(self.dataPointList))
                        payload = "{" + "\"items\": ["
                        for dataPoint in self.dataPointList:
                                payload += str(dataPoint) + ","
                        payload += "] }"
                        print("payload=",payload)
                        apiCmd = "/api/publish/Cjm/PublicUpdateDataPointCommand"
                        result = fct_callCJM(apiCmd, payload)
                        #self.fct_displayResultAppend(result)
       
        except ValueError:
            pass

    
    def fct_loadExcelData(self):
        excelfile = cloudAccount.excelFileName
        logging.debug(f'excelfile={excelfile}')
        self.fct_displayInfoAppend("Excel file Name="+excelfile)
        
        if excelfile == "DIALOG":
            filenameXLSX = QFileDialog.getOpenFileName(
                None, "Choose a file", cloudAccount.dirCSVRoot,
                filter="Excel file (*.xlsx)")
            filenameXLSX = filenameXLSX[0]
        else:
            filenameXLSX = cloudAccount.dirCSVRoot + "/" + excelfile
        logging.debug(f'filename CSV={filenameXLSX}')
        if filenameXLSX == "":
            logging.debug(f'No file selected')
            return

        # process all SHEETS of the excel file
        f = pandas.ExcelFile(filenameXLSX)
        logging.debug(f'sheetnames={f.sheet_names}')
        payload = "{\n \"items\": [  \n "
        for sheetName in f.sheet_names:
            if (sheetName == "DATAPOINTS"):
                logging.debug(f'sheetname for DATAPOINTS')
                self.fct_displayInfoAppend(f'==> load DATAPOINTS from Excel:')
                excel_data_df = pandas.read_excel(filenameXLSX, sheet_name=sheetName)
                dataPointValue = excel_data_df.to_json(orient='records')
                #print('Excel Sheet to JSON:\n', dataPointValue)
                logging.debug(f'dataPointValue = {dataPointValue}')
                dataPointValue = json.loads(dataPointValue)
                #print('dataPointValue type:\n', type(dataPointValue))
                for metric in dataPointValue:
                    logging.debug(f'metric = {metric}')
                    # print("metric=", str(metric))
                    #print('metric.datasetkey:\n', metric["datasetkey"])
                    payload = payload + "{" \
                                + "  \"dataPointKey\": " + "\"" + metric["datapointkey"]           + "\"" + "," \
                                + "  \"dataSetKey\":   " + "\"" + metric["datasetkey"]             + "\"" + "," \
                                + "  \"unit\":         " + "\"" + metric["datapointunit"]          + "\"" + "," \
                                + "  \"value\":        " + "\"" + str(metric["datapointvalue"])    + "\""       \
                                + " }"
                    #print("dataPointValue.index(metric)",dataPointValue.index(metric))
                    #print("len(dataPointValue)",len(dataPointValue))
                    logging.debug(f'payload={payload}')
                    if dataPointValue.index(metric) != (len(dataPointValue)-1):   # if not last item
                        payload = payload + ","
            else:
                logging.debug(f'sheetName = {sheetName}')
                self.fct_displayInfoAppend(f'==> fct_loadExcelData({sheetName})')
                excel_data_df = pandas.read_excel(filenameXLSX, sheet_name=sheetName)
                dataSet, dataPoint = sheetName.split("_",1)
                logging.debug(f'dataSet = {dataSet}')
                logging.debug(f'dataPoint = {dataPoint}')
                dataPointValue = excel_data_df.to_json(orient='records')
                logging.debug(f'Excel Sheet to JSON:\n {dataPointValue}')
                payload = payload + "{" \
                                    + "  \"dataPointKey\": " + "\"" + dataPoint           + "\"" + "," \
                                    + "  \"dataSetKey\":   " + "\"" + dataSet             + "\"" + "," \
                                    + "  \"value\":        " +        str(dataPointValue)              \
                                    + " }"
            if f.sheet_names.index(sheetName) != (len(f.sheet_names)-1):   # if not last sheet
                payload = payload + ","
        payload = payload +" \n ] \n } \n\n"
        logging.debug(f'payload={payload}')
        
        apiCmd = "/api/publish/Cjm/PublicUpdateDataPointCommand"
        result = fct_callCJM(apiCmd, payload)
        self.fct_displayResultAppend(result)
        logging.debug(f'result= {result}')

    
    
    def fct_removeItemsFromMenuEnv(self):
        self.envMenu.clear()

    def fct_addMenuEnv(self):
        self.envMenu.clear()
        listCloudAccount = cloudAccount.getListCloudAccount(
            cloudAccount.filenameCFGFullPath)
        for account in listCloudAccount:
            actionChangeCloudAccount = QAction(
                QIcon(fct_imageFullPath('changecloudaccount.png')), "change to "+account, self)
            actionChangeCloudAccount.triggered.connect(
                self.fct_changeCloudAccount)
            actionChangeCloudAccount.setData(account)
            self.envMenu.addAction(actionChangeCloudAccount)

    def fct_loadCfgScriptDialog(self):
        print("==>fct_loadCfgScriptDialog()")
        cloudAccount.filenameCFG = QFileDialog.getOpenFileName(
            None, "Choose a config file", cloudAccount.configPath,
            filter="Environment CFG file (*.cfg)")
        cloudAccount.filenameCFGFullPath = cloudAccount.filenameCFG[0]
        print("cloudAccount.filenameCFGFullPath=",
              cloudAccount.filenameCFGFullPath)
        self.fct_addMenuEnv()
        cloudAccount.setNewEnv(cloudAccount.filenameCFGFullPath, "default")
        self.updateUI()

    def fct_changeCloudAccount(self):
        print("==> fct_changeCloudAccount()")
        action = self.sender()
        print("fct_changeCloudAccount action=", action)
        print("data=", action.data())
        newEnvName = action.data()
        print("cloudAccount.filenameCFGFullPath=",
              cloudAccount.filenameCFGFullPath)
        cloudAccount.setNewEnv(cloudAccount.filenameCFGFullPath, newEnvName)
        self.updateUI()

    def fct_credits(self):
        dlg = QMessageBox()
        dlg.setWindowTitle("About CJM Demo toolkit")
        dlg.setText(
            "CJM Demo ToolKit Version " + str(VERSION) + " \n Quadient 2020 \n Developer: Laurent Ghio \n Coded in Python PyQt5 \n Thanks")
        dlg.exec_()


def main():
    global cloudAccount
    cloudAccount = classCloudAccount()

    # determine if application is a script file or frozen exe via Pyinstaller
    if getattr(sys, 'frozen', False):
        applicationPath = os.path.dirname(sys.executable)
        print("frozen")
    elif __file__:
        applicationPath = sys.path[0]
        # applicationPath = os.path.dirname(__file__)
        print("not frozen")
        print("sys.path[0]=", sys.path[0])

    print('applicationPath=', applicationPath)
    print("sys.argv[0]=", sys.argv[0])

    cloudAccount.applicationPath = applicationPath
    cloudAccount.imagePath = applicationPath + '/images/'
    cloudAccount.configPath = applicationPath + '/config/'
    cloudAccount.logFile = os.path.splitext(sys.argv[0])[0] + '.log'
    print("cloudAccount.applicationPath=", cloudAccount.applicationPath)
    print("cloudAccount.imagesPath=", cloudAccount.imagePath)
    print("cloudAccount.configPath=", cloudAccount.configPath)
    print("cloudAccount.logFile=", cloudAccount.logFile)
    # sys.exit(0)

    # Open the Log file
    #logging.basicConfig(filename=cloudAccount.logFile, level=logging.DEBUG,
    #                    format='%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s] %(message)s',
    #                    datefmt='%Y-%m-%d,%H:%M:%S',
    #                    filemode='w'
    #                    )
    logger = logging.getLogger('')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(cloudAccount.logFile)
    formatter = logging.Formatter('[%(asctime)s.%(msecs)03d] %(levelname)s [{%(module)s} %(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%Y-%m-%d,%H:%M:%S')
    fh.setFormatter(formatter)
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(colorlog.ColoredFormatter('%(log_color)s [%(asctime)s.%(msecs)03d] %(levelname)s [{%(module)s} %(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%Y-%m-%d,%H:%M:%S'))
    logger.addHandler(fh)
    logger.addHandler(sh)

    def hello_logger():
        logger.info("Hello info")
        logger.critical("Hello critical")
        logger.warning("Hello warning")
        logger.debug("Hello debug")
        logger.error("Error message")
    hello_logger()

    # if the cjmdemotoolkit.cfg file exists in the config directory
    # load the [default] section
    defaultConfigFile = cloudAccount.configPath + "cjmdemotoolkit.cfg"
    if os.path.isfile(defaultConfigFile):
        cloudAccount.filenameCFGFullPath = defaultConfigFile
        print("default config file exists", defaultConfigFile)
        cloudAccount.setNewEnv(defaultConfigFile, "default")
    # sys.exit(0)

    QCoreApplication.setApplicationName("CJM Demo")
    QCoreApplication.setApplicationVersion("0.0.2")
    QCoreApplication.setOrganizationName("Quadient")
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle(QStyleFactory.create("Fusion"))
    app.setStyleSheet("""
        QPushButton:hover { color: #45bbe6 }
        __QToolBar{background:  # 222222; }
        __QScrollBar: vertical {border: grey; background:  # 111122;}
        __QScrollBar: : add-line: vertical {background: red; }
        __QScrollBar: : sub-line: vertical {background: yellow; }
        QComboBox{background:  # 2A2A33; }
        QComboBox: : item: selected {background: white}
        __QComboBox:: editable {color: white }
        QDockWidget#Parameters {color: red; }

        """)

    p = app.palette()
    p.setColor(QPalette.Window, QColor(53, 53, 53))
    p.setColor(QPalette.WindowText, Qt.white)
    p.setColor(QPalette.Base, QColor(64, 66, 68))
    p.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    p.setColor(QPalette.ToolTipBase, Qt.white)
    p.setColor(QPalette.ToolTipText, Qt.black)
    p.setColor(QPalette.Text, Qt.white)
    p.setColor(QPalette.Button, QColor(53, 53, 69))
    p.setColor(QPalette.ButtonText, Qt.white)
    p.setColor(QPalette.BrightText, Qt.red)
    p.setColor(QPalette.Highlight, QColor("#45bbe6"))
    p.setColor(QPalette.HighlightedText, Qt.black)
    p.setColor(QPalette.Disabled, QPalette.Text, Qt.darkGray)
    p.setColor(QPalette.Disabled, QPalette.ButtonText, Qt.darkGray)
    p.setColor(QPalette.Link, QColor("#bbb"))
    app.setPalette(p)
    app.setWindowIcon(QIcon(fct_imageFullPath("logo.svg")))

    win = MainWindow(app)
    win.updateUI()
    win.show()

    ret = app.exec()
    sys.exit(ret)


if __name__ == "__main__":
    main()

